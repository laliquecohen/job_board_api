<?php
class Applicant
extends Eloquent
{
    protected $table = "applicants";
 
    protected $guarded = [
        "first_name",
		"last_name",
		"email",
		"notice_days",
		"education",
		"work_experience",
		"job_id",
        "created_at",
    ];
	
}