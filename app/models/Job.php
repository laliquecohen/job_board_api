<?php
class Job
extends Eloquent
{
    protected $table = "jobs";
 
    protected $guarded = [
        "id",
		"title",
		"org_name",
		"org_email",
		"salary",
		"description",
        "created_at",
    ];
	
}