<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('applicants', function($table)
		{
			$table->increments('id');
			$table->integer('job_id');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('email');
			$table->integer('notice_days');
			$table->longText('education');
			$table->longText('work_experience');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('applicants');
	}

}
