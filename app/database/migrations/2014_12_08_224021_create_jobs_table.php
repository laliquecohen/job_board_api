<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jobs', function($table)
		{
			$table->increments('id');
			$table->string('title');
			$table->string('org_name');
			$table->string('org_email');
			$table->float('salary');
			$table->longText('description');
			$table->dateTime('created_at');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('jobs');
	}

}
