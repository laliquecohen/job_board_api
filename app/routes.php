<?php


Route::get('/', function()
{
	return View::make('hello');
});
Route::resource('jobs', 'JobController');
Route::resource('applicants', 'ApplicantController');

Route::get("jobs/{jobs}", [
    "as"   => "jobs/search",
    "uses" => "JobController@search"
]);
Route::get("jobs/{jobs}", [
    "as"   => "jobs/orderBy",
    "uses" => "JobController@orderBy"
]);
Route::get("applicants/{applicants}", [
    "as"   => "applicants/apply",
    "uses" => "ApplicantController@store"
]);