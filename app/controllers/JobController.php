<?php

class JobController extends BaseController {

	public $restful = true;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$jobs = Job::get();
		return array($jobs);
		
	}
	
	/**
	 * Search by title, org_name, org_email
	 *
	 * @return Response
	 */
	public function search()
	{
		$job = new Job();
		
		$title = Input::get('title');
		$org_name = Input::get('org_name');
		$org_email = Input::get('org_email');
		if ($title) {
			$job = Job::where('title', $title)->get();
			return array($job);
		}
		if ($org_name) {
			$job = Job::where('org_name', $org_name)->get();
			return array($job);
		}
		if ($org_email) {
			$job = Job::where('org_email', $org_email)->get();
			return array($job);
		}
		return;
		
	}

	/**
	 * order by date
	 *
	 * @return Response
	 */
	public function orderBy()
	{

		$jobs = Job::orderBy('created_at', 'desc')->get();
		return array($jobs);
		
	}



	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$title = Input::get('title');
		$job = new Job();
		$job->title = Input::get('title');
		$job->org_name = Input::get('org_name');
		$job->org_email = Input::get('org_email');
		$job->salary = Input::get('salary');
		$job->description = Input::get('description');
	
		$job->save();

	}




}
