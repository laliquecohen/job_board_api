<?php

class ApplicantController extends BaseController {

	public $restful = true;
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $job_id = Input::get('job_id');
		if ($job_id) {
			$applicants = Applicant::where('job_id', $job_id)->get();
		} else {
			$applicants = Applicant::all();
		}
		return array($applicants);
		
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$applicant = new Applicant();
		$applicant->first_name = Input::get('first_name');
		$applicant->last_name = Input::get('last_name');
		$applicant->email = Input::get('email');
		$applicant->notice_days = Input::get('notice_days');
		$applicant->education = Input::get('education');
		$applicant->work_experience = Input::get('work_experience');
		$applicant->job_id = Input::get('job_id');
	
		$applicant->save();

	}



}
